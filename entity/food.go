package entity

type Food struct {
	Model
	Name        string
	Price       int
	Description string
}
