# go-template


using go version 1.16 with go mod

1. install package with : "go get ./.."
2. set go env to: "GO111MODULE=auto"
3. run with: "go run main.go" or "go run ."

## Swagger Installation
1. run ```swag init``` in command line
2. after changes somethig about this documentation run ```swag init``` again before running the server